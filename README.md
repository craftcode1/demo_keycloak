# Creating Keycloak with PostgresDB

Based on [documentation of keycloak image](https://hub.docker.com/r/jboss/keycloak).

## Docker network 
    docker network create keycloak-network

## [Postgres instance](https://hub.docker.com/_/postgres/)

    docker pull postgres

    docker run --name postgres -p 5432:5432 --net keycloak-network -e POSTGRES_DB=keycloak -e POSTGRES_USER=craftcode -e POSTGRES_PASSWORD=craftcode -d postgres

## [Keycloak instance](https://hub.docker.com/r/jboss/keycloak)

    docker pull jboss/keycloak

    docker run --name keycloak -d -p 8089:8080 --net keycloak-network -e DB_VENDOR=POSTGRES -e DB_ADDR=postgres -e DB_USER=craftcode -e DB_PASSWORD=craftcode -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin jboss/keycloak

## [Setup Keycloak](https://www.baeldung.com/spring-boot-keycloak)

I used PGAdmin to check if the users were copied in the PostgresDB. 

![SELECT * FROM user_entity](img/keycloak_users_postgres.png)

## [Setup Spring app](https://www.baeldung.com/spring-boot-keycloak)