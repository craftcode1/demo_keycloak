package be.craftcode.demo.controllers;

import be.craftcode.demo.model.MovieQuote;
import be.craftcode.demo.service.interfaces.IMovieQuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class MovieLineController {
    private final IMovieQuoteService movieQuoteService;

    @Autowired
    public MovieLineController(IMovieQuoteService movieQuoteService) {
        this.movieQuoteService = movieQuoteService;
    }

    @RequestMapping(value = "/api/quote",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MovieQuote>> findAllQuotes() {
        try {
            return new ResponseEntity<>(movieQuoteService.quotes(), HttpStatus.OK);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
