package be.craftcode.demo.service.impl;

import be.craftcode.demo.model.MovieQuote;
import be.craftcode.demo.repo.MovieRepo;
import be.craftcode.demo.service.interfaces.IMovieQuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieQuoteImpl implements IMovieQuoteService {
    private final MovieRepo repo;

    @Autowired
    public MovieQuoteImpl(MovieRepo repo) {
        this.repo = repo;
    }

    @Override
    public List<MovieQuote> quotes() {
        return repo.getMovieQuoteList();
    }
}
