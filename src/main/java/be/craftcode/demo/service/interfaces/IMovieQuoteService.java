package be.craftcode.demo.service.interfaces;

import be.craftcode.demo.model.MovieQuote;

import java.util.List;

public interface IMovieQuoteService {
    List<MovieQuote> quotes();
}
