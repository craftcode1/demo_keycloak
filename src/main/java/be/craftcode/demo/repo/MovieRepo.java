package be.craftcode.demo.repo;

import be.craftcode.demo.model.MovieQuote;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MovieRepo {
    private final List<MovieQuote> movieQuoteList;

    public MovieRepo() {
        this.movieQuoteList = List.of(
                MovieQuote.builder().quote("Say hello to my little friend!").build(),
                MovieQuote.builder().quote("I am your father.").build(),
                MovieQuote.builder().quote("Do you feel lucky, punk.").build(),
                MovieQuote.builder().quote("Funny how, funny like a clown?").build(),
                MovieQuote.builder().quote("Do they speak english in What?").build(),
                MovieQuote.builder().quote("ET phone home.").build()
        );
    }

    public List<MovieQuote> getMovieQuoteList() {
        return movieQuoteList;
    }
}
